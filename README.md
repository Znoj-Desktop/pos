### **Description**

Zadání: [Projekt návrhu sítě](./README/Projekt_PS_1314Z.pdf)  

|-                               |-                                   |-               | 
|--------------------------------|------------------------------------|----------------|  
|Číslo topologie:                |F                                   |  
|Čísla VLAN:                     |VLAN A=125, VLAN B=173              |  
|Počty stanic na segmentech:     |VLAN A=160, VLAN B=11, Segment S1=98|  
|Rozsah veřejných adres:         |150.140.200.0/21                    |  
|Rozsah privátních adres:        |10.210.111.224/27                   |  
|Rozsah IPv6 adres:              |2002:a819:3596::/48                 |  
|Zvláštní segmenty:              |                                    |  
|                                |NAT:                                |VLAN B,         |  
|                                |DNS:                                |Segment S1 (PC3)|  
|                                |DHCP:                               |VLAN A,         |  
|                                |T:                                  |VLAN A,         |  
|                                |N:                                  |VLAN A          |  
|NAT pool:                       |22                                  |  
|Směrovací protokol:             |OSPF                                |  

![](./README/Lokalita-ISP.png)  
![](./README/pos.png)  

---
### **Technology**
Network

---
### **Year**
2013

---
### **Solution**
#### 1. Adresní plán a konfigurace VLAN 
##### Převod na L3 
![](./README/jenL3.png)  

##### Subneting 
![](./README/posL3_bez_adres.png)  
###### Kompletní adresní plán pro IPv4 
|Subnet 	    |Hostů 	|Celkem 	|Rozsah 	Maska 	|IP síť 	          |IP broadcast    |
|-------------|-------|---------|-----------------|-------------------|----------------|
|A 	        	|160 		|163 			|256 			/24 		|150.140.200.0 	    |150.140.200.255 | 
|B 	        	|11 	  |14 	    |16 	    /28 		|10.210.111.225 	  |10.210.111.239  |
|C 	        	|98 	  |101 			|128 			/25 		|150.140.201.128 		|150.140.201.255 | 
|NAT pool 		|22 	  |25 	    |32 	    /27 		|150.140.201.32 	  |150.140.201.63  |
|D 	       	 	|0 	    |4 	    	|4 	    	/30 		|150.140.201.4 	    |150.140.201.7   |
|E 	       	 	|0 	    |4 	    	|4 	    	/30 		|150.140.201.0 	    |150.140.201.3   |
|R1-ISP 	    |0 	    |4 	    	|4 	    	/30 		|10.0.0.0 	        |10.0.0.3        |

###### Kompletní adresní plán pro IPv6  
|Subnet 	|Maska 	|IP síť 	            |IP poslední stanice                  |
----------|-------|---------------------|-------------------------------------|
|A 	    	|/64 		|2002:a819:3596:1:: 	|2002:a819:3596:1:ffff:ffff:ffff:ffff | 
|B 	    	|/64 		|2002:a819:3596:2:: 	|2002:a819:3596:2:ffff:ffff:ffff:ffff | 
|C 	    	|/64 		|2002:a819:3596:3:: 	|2002:a819:3596:3:ffff:ffff:ffff:ffff | 
|D 	    	|/64 		|2002:a819:3596:4:: 	|2002:a819:3596:4:ffff:ffff:ffff:ffff | 
|E 	    	|/64 		|2002:a819:3596:5:: 	|2002:a819:3596:5:ffff:ffff:ffff:ffff | 
|R1-ISP 	|/64 		|2002:a819:3596:6:: 	|2002:a819:3596:6:ffff:ffff:ffff:ffff | 

##### Přidělení adres zařízením/rozhraním  
|Zařízení 		|Rozhraní 	    |VLAN 		|Subnet 	|IPv4 	            |IPv6  																|
|-------------|---------------|---------|---------|-------------------|-------------------------------------|
|R1 	        |Gi0/0 (ISP) 		|- 	    	|R1-ISP 	|10.0.0.2 	        |2002:a819:3596:6::2                  |
|							|Ser0/0 (R2) 		|- 	    	|D 	    	|150.140.201.5 	    |2002:a819:3596:4::1                  |
|							|Ser0/1 (R3) 		|- 	    	|E 	    	|150.140.201.1 	    |2002:a819:3596:5::1                  |
|R2 	        |Ser0/0 (R1) 		|- 	    	|D 	    	|150.140.201.6 	    |2002:a819:3596:4::2                  |
|							|Gi0/0 	      	|C 	    	|C 	    	|150.140.201.129 		|2002:a819:3596:3::1                  |
|							|Gi0/1 	      	|A 	    	|A 	    	|150.140.200.1 	    |2002:a819:3596:1::1                  |
|							|Gi0/2 	      	|B 	    	|B   			|10.210.111.226 	  |2002:a819:3596:2::1                  |
|R3 	        |Ser0/0 (R1) 		|- 	    	|E   			|150.140.201.2 	    |2002:a819:3596:5::2                  |
|							|Gi0/0 	      	|B 	    	|B 	    	|10.210.111.227 	  |2002:a819:3596:2::2                  |
|PC1A    			|Eth0 	        |A 	    	|A 	    	|150.140.200.254 		|2002:a819:3596:1:ffff:ffff:ffff:ffff | 
|PC2A 	    	|Eth0 	        |A 	    	|A 	    	|150.140.200.253 		|2002:a819:3596:1:ffff:ffff:ffff:fffe | 
|PC1B 	    	|Eth0 	        |B 	    	|B   			|10.210.111.238 	  |2002:a819:3596:2:ffff:ffff:ffff:ffff | 
|PC2B 	    	|Eth0 	        |B 	    	|B   			|10.210.111.237 	  |2002:a819:3596:2:ffff:ffff:ffff:fffe | 
|PC3 	    		|Eth0        		|C 	    	|C   			|150.140.201.254 		|2002:a819:3596:3:ffff:ffff:ffff:ffff | 
|DNS server 	|Eth0 	        |C 	    	|C 	    	|150.140.201.254 		|2002:a819:3596:3:ffff:ffff:ffff:ffff | 

##### Obrázek překreslené logické topologie včetně všech adres  
![](./README/pos.png)  

##### Obrázek fyzické topologie včetně adres podsítí, rozhraní směrovačů a počítačů  
![](./README/posL3.png)  